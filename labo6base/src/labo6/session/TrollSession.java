package labo6.session;

import labo6.Labo6Main;
import labo6.User;
import labo6.profilers.Profiler;
import labo6.profilers.TrollProfiler;

public class TrollSession extends Session {

	private User user;
	
	public TrollSession(Labo6Main l, User u) {
		super(l, u);
		user = u;
	}
	
	@Override
	protected Profiler createProfiler() {

		return new TrollProfiler(this, user);
	}

}
