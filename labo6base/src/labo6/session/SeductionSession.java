package labo6.session;

import labo6.Labo6Main;
import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.CheckUserBehaviorDontCare;
import labo6.bots.behavior.WaitBehavior;
import labo6.bots.behavior.WaitBehaviorSaySomething;
import labo6.database.Picture;
import labo6.database.Picture.PictureKey;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextDatabase;
import labo6.database.TextList;
import labo6.database.TextMessage.TextKey;
import labo6.profilers.Profiler;
import labo6.profilers.SeductiveProfile;

public class SeductionSession extends Session {

	private User user;
	
	public SeductionSession(Labo6Main l, User u) {
		super(l, u);
		user = u;
	}
	
	@Override
	protected Profiler createProfiler() {

		return new SeductiveProfile(this, user);
	}
}
