package labo6.session;

import labo6.Labo6Main;
import labo6.User;
import labo6.profilers.CasualProfile;
import labo6.profilers.Profiler;

public class CasualSession extends Session {
	
	private User user;

	public CasualSession(Labo6Main l, User u) {
		super(l, u);
		user = u;
	}

	@Override
	protected Profiler createProfiler() {

		return new CasualProfile(this, user);
	}

}
