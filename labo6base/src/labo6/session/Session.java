package labo6.session;

import labo6.Labo6Main;
import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.CheckUserBehaviorQuestion;
import labo6.bots.behavior.WaitBehavior;
import labo6.bots.behavior.WaitBehaviorAsk;
import labo6.database.Picture;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextDatabase;
import labo6.database.TextList;
import labo6.database.TextMessage.TextKey;
import labo6.profilers.NormalProfile;
import labo6.profilers.Profiler;


/*
 * Cette classe repr�sente une session d'un utilisateur humain
 * avec un ou plusieurs robots.
 * La session se termine lorsqu'on d�tecte que l'utilisateur humain
 * s'est d�connect� (change de pays ou de genre, via les boutons "PAYS" et "GENRE").
 */

public class Session {

	private User human;
	private ChatBot robot;
	private Labo6Main ui;
	private boolean ended;
	private Thread sleeper;
	private Profiler profiler;
	
	public static final String NORMAL_SESSION = "normal"; 
	public static final String SEDUCTION_SESSION = "seduction";
	public static final String CASUAL_SESSION = "casual";
	public static final String TROLL_SESSION = "troll";

	public Session(Labo6Main l, User u) {
		ui = l;
		human = u;
		ended = false;
		sleeper = Thread.currentThread();
	}
	
	public static Session createSession(String type, Labo6Main ui, User humanUser) {
		
		if(type.equals(NORMAL_SESSION)){
			return new Session(ui,humanUser);
		}
		else if (type.equals(SEDUCTION_SESSION)) {
			return new SeductionSession(ui,humanUser);
		}
		else if (type.equals(CASUAL_SESSION)) {
			return new CasualSession(ui,humanUser);
		}
		else if (type.equals(TROLL_SESSION)) {
			return new TrollSession(ui, humanUser);
		}
		else
		{
			throw new IllegalArgumentException ("Wrong session type: "+type);
		}
	
	}

	public void start() {
		
		profiler = createProfiler();

		robot = profiler.createChatBot(human, "Other", profiler.getSuitablePictures().random(), Gender.random());
		ui.initBackGround(robot);
		
		robot.appendMessage(profiler.generateGreeting());
		while (!hasEnded()) {

			waitForUser();

			if (robot.wakeUp()) {

				robot.appendMessage(profiler.generateAnswer());
			}

		}

	}

	private void waitForUser() {
		robot.waitForUser();
	}
	
	protected Profiler createProfiler() {
		
		return new NormalProfile(this, human);
		
	}

	/*
	 * Appel� par le bouton SUIVANT
	 */
	public void changeChatBot() {
		robot = profiler.createChatBot(human, "Other", profiler.getSuitablePictures().random(), Gender.random());
		ui.initBackGround(robot);
	}
	

	public synchronized void end() {
		ended = true;
		sleeper.interrupt();
	}

	private synchronized boolean hasEnded() {
		return ended;
	}	
	
	

}
