package labo6.bots.behavior;

public class CheckUserBehaviorQuestion extends CheckUserBehavior {
	
	@Override
	public boolean checkForWakeUp() {
		
		return bot.getPeer().getLastLine().contains("?");
		
	}

}
