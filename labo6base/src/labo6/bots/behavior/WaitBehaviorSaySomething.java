package labo6.bots.behavior;

public class WaitBehaviorSaySomething extends WaitBehavior {

	@Override
	public void waitForUser() {
		bot.sleep(1000);
		bot.appendMessage(bot.getProfiler().getSuitableMessages().random().getMessage());
	}

}
