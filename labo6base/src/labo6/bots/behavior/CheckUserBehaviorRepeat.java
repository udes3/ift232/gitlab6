package labo6.bots.behavior;

public class CheckUserBehaviorRepeat extends CheckUserBehavior {

	private String lastLine = "";

	@Override
	public boolean checkForWakeUp() {
		
		boolean res = false;
		
		res = bot.getPeer().getLastLine().equals(lastLine);
		lastLine = bot.getPeer().getLastLine();
		
		return res;
	}
	
	

}
