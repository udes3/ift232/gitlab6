package labo6.bots.behavior;

import labo6.database.TextList;
import labo6.database.TextMessage.TextKey;

public class WaitBehaviorAsk extends WaitBehavior {

	@Override
	public void waitForUser() {
		bot.sleep(3000);
		TextList list = bot.getProfiler().getSuitableMessages();
		list.keep(TextKey.isQuestion, true);
		bot.appendMessage(list.random().getMessage());
	}

}
