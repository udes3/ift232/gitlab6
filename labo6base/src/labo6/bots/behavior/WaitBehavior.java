package labo6.bots.behavior;

import labo6.bots.ChatBot;
import labo6.profilers.Profiler;

public abstract class WaitBehavior {

	protected ChatBot bot;
	protected Profiler profiler;
	
	public void setBot(ChatBot b) {
		bot = b;
	}
	
	abstract public void waitForUser();
}
