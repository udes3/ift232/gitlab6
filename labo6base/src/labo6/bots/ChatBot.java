package labo6.bots;

import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.WaitBehavior;
import labo6.database.Picture;
import labo6.profilers.Profiler;
import labo6.session.Session;

public class ChatBot extends User {

	
	// L'utilisateur avec lequel le robot est en communication.
	protected User peer;
	protected String oldText = "";
	protected Session session;
	protected CheckUserBehavior check;
	protected WaitBehavior wait;
	protected Profiler profiler;
	
	public ChatBot(Session s,Profiler pro, CheckUserBehavior c, WaitBehavior w, User p, String n, Picture pic, Gender g) {
		super(n, pic, g);
		peer = p;
		session = s;
		check = c;
		check.setBot(this);
		wait = w;
		wait.setBot(this);
		profiler = pro;
	}

	public void sleep(int time) {
		try {

			Thread.sleep(time);
		} catch (InterruptedException e) {
		}
	}

	public void appendMessage(String msg) {
		getUI().appendMessage(msg);
	}

	public User getPeer() {
		return peer;
	}

	public boolean wakeUp() {

		boolean res = false;
		if (!peer.getText().equals(oldText)) {
			
			res = check.checkForWakeUp();
		}

		oldText = peer.getText();
		return res;
	}

	

	public void waitForUser() {
		
		wait.waitForUser();
	}

	public Session getSession() {
		
		return session;
	}
	
	public Profiler getProfiler() {
		return profiler;
	}

}
