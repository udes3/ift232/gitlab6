package labo6.profilers;

import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.CheckUserBehaviorQuestion;
import labo6.bots.behavior.WaitBehavior;
import labo6.bots.behavior.WaitBehaviorAsk;
import labo6.database.Picture;
import labo6.database.Picture.PictureKey;
import labo6.session.Session;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextDatabase;
import labo6.database.TextList;

public class NormalProfile extends Profiler {
	
	private Session session;
	private User user;

	public NormalProfile(Session s, User human) {
		
		session = s;
		user = human;
	}

	public PictureList getSuitablePictures() {
		return PictureDatabase.getAllPictures();
	}

	public TextList getSuitableMessages() {
		
		TextList list = TextDatabase.getAllMessages();
		selectLanguage(user, list);
		return list;
	}

	protected WaitBehavior createWaitBehavior() {
		return new WaitBehaviorAsk();
	}

	protected CheckUserBehavior createCheckBehavior() {
		return new CheckUserBehaviorQuestion();

	}

	public ChatBot createChatBot(User human, String name, Picture pic, Gender gen) {
		
		PictureList list = getSuitablePictures();
		list.keep(PictureKey.gender, gen);
		getSuitablePhotos(user, list);
		pic = list.random();

	
		return new ChatBot(session,this, createCheckBehavior(), createWaitBehavior(), human, name, pic, gen);
	}

}
