package labo6.profilers;

import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.CheckUserBehaviorDontCare;
import labo6.bots.behavior.WaitBehavior;
import labo6.bots.behavior.WaitBehaviorSaySomething;
import labo6.database.Picture;
import labo6.database.Picture.PictureKey;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextDatabase;
import labo6.database.TextList;
import labo6.database.TextMessage.TextKey;
import labo6.session.Session;

public class SeductiveProfile extends Profiler {

	private Session session;
	private User user;

	public SeductiveProfile(Session s, User human) {
		session = s;
		user = human;
	}

	@Override
	public PictureList getSuitablePictures() {
		PictureList list = PictureDatabase.getAllPictures();
		list.keep(PictureKey.isSeductive, true);

		return list;
	}

	@Override
	public TextList getSuitableMessages() {
		TextList list = TextDatabase.getAllMessages();
		list.keep(TextKey.isSeductive, true);
		selectLanguage(user, list);

		return list;
	}

	@Override
	public ChatBot createChatBot(User human, String name, Picture pic, Gender gen) {
		
		switch(human.userGender) {
		case male:
			gen = Gender.female;
			break;
		case female:
			gen = Gender.male;
			break;
		case unknown:
			gen = Gender.unknown;
			break;
		}
		
		PictureList list = getSuitablePictures();
		list.keep(PictureKey.gender, gen);
		getSuitablePhotos(user, list);
		pic = list.random();
		
		return new ChatBot(session,this, createCheckBehavior(), createWaitBehavior(), human, name, pic, gen);
	}

	@Override
	protected CheckUserBehavior createCheckBehavior() {
		return new CheckUserBehaviorDontCare();

	}

	@Override
	protected WaitBehavior createWaitBehavior() {
		return new WaitBehaviorSaySomething();

	}
}
