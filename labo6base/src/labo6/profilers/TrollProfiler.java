package labo6.profilers;

import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.CheckUserBehaviorDontCare;
import labo6.bots.behavior.CheckUserBehaviorQuestion;
import labo6.bots.behavior.WaitBehavior;
import labo6.bots.behavior.WaitBehaviorSaySomething;
import labo6.database.Picture;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextDatabase;
import labo6.database.TextList;
import labo6.database.Picture.PictureKey;
import labo6.database.TextMessage.Language;
import labo6.database.TextMessage.TextKey;
import labo6.session.Session;

public class TrollProfiler extends Profiler {

	private Session session;
	private User user;
	private TextList list;
	private Gender botGender;
	private int compteur = 0;

	public TrollProfiler(Session s, User human) {
		session = s;
		user = human;
		
	}
	
	@Override
	public void selectLanguage(User human, TextList list) {
		switch(human.getCountry()) {
		case Quebec:
			list.keep(TextKey.language, Language.english);
			break;
		case France:
			list.keep(TextKey.language, Language.english);
			break;
		default:
			list.keep(TextKey.language, Language.french);
			
		}
	}

	@Override
	public TextList getSuitableMessages() {
		
		list = TextDatabase.getAllMessages();
		if(user.userGender == botGender) {
			if(compteur > 10) {
				list.keep(TextKey.isPassiveAggressive, true);
			} else {
			list.keep(TextKey.isSeductive, true);
			compteur++;
			}
		} 
		else if(compteur > 10) {
			list.keep(TextKey.isPassiveAggressive, true);
		} else {
			list.remove(TextKey.isSeductive, true);
			compteur++;
		}
		
		
		selectLanguage(user, list);

		return list;
	}

	@Override
	protected WaitBehavior createWaitBehavior() {
		return new WaitBehaviorSaySomething();
	}

	@Override
	protected CheckUserBehavior createCheckBehavior() {
		return new CheckUserBehaviorDontCare();
	}
	
	@Override
	public String generateGreeting() {
		TextList list = getSuitableMessages();
		list.keep(TextKey.isOffensive, true);

		return list.random().getMessage();
	}

	@Override
	public String generateAnswer() {
		
		compteur = 0;
		TextList listAnswer = getSuitableMessages();
		listAnswer.keep(TextKey.isOffensive, true);
		return (listAnswer.random().getMessage());
	}

	@Override
	public ChatBot createChatBot(User human, String name, Picture pic, Gender gen) {
		PictureList list = getSuitablePictures();
		list.keep(PictureKey.gender, gen);
		getSuitablePhotos(user, list);
		pic = list.random();
		botGender = gen;
		
		
		return new ChatBot(session,this, createCheckBehavior(), createWaitBehavior(), human, name, pic, gen);
	}

	@Override
	public PictureList getSuitablePictures() {
		return PictureDatabase.getAllPictures();
	}

}
