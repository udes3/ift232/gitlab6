package labo6.profilers;

import labo6.Ressources.Country;
import labo6.Ressources.Gender;
import labo6.User;
import labo6.bots.ChatBot;
import labo6.bots.behavior.CheckUserBehavior;
import labo6.bots.behavior.WaitBehavior;
import labo6.database.Picture;
import labo6.database.Picture.PictureKey;
import labo6.database.PictureDatabase;
import labo6.database.PictureList;
import labo6.database.TextList;
import labo6.database.TextMessage.Language;
import labo6.database.TextMessage.TextKey;

public abstract class Profiler {
	
	public String generateGreeting() {
		TextList list = getSuitableMessages();
		list.keep(TextKey.isGreeting, true);

		return list.random().getMessage();
	}

	public String generateAnswer() {
		return (getSuitableMessages().random().getMessage());
	}
	
	public void selectLanguage(User human, TextList list) {
		switch(human.getCountry()) {
		case Quebec:
			list.keep(TextKey.language, Language.french);
			break;
		case France:
			list.keep(TextKey.language, Language.french);
			break;
		default:
			list.keep(TextKey.language, Language.english);
			
		}
	}

	public void getSuitablePhotos(User human, PictureList list) {
		
		if(human.getCountry() == Country.Japan) {
			list.keep(PictureKey.isComic, true);
		}
	}
	
	public abstract PictureList getSuitablePictures();

	public abstract TextList getSuitableMessages();

	protected abstract WaitBehavior createWaitBehavior();

	protected abstract CheckUserBehavior createCheckBehavior();

	public abstract ChatBot createChatBot(User human, String name, Picture pic, Gender gen);

}
